#include "tableview.h"

//-------------------------------------------------------------------------------------------------------
//                                           SteelChemTable
//-------------------------------------------------------------------------------------------------------

QVariant SteelChemTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Orientation::Vertical) {
        return QString(chemNames[section]);
    }
    return QVariant();
}

QVariant SteelChemTable::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    return chemData[index.row()];
}

bool SteelChemTable::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid() || role != Qt::EditRole)
        return false;

    bool ok = false;
    double newVal = value.toDouble(&ok);
    if (!ok || newVal < 0)
        return false;

    chemData[index.row()] = newVal;

    return true;
}

//-------------------------------------------------------------------------------------------------------
//                                           CCTExperimentTable
//-------------------------------------------------------------------------------------------------------

QVariant CCTExperimentTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Orientation::Horizontal)
    {
        switch (section) {
        case WcCol: return "Wc";
        case HVCol: return "HV";
        case FPCol: return "FP";
        case BCol:  return "B";
        case MCol:  return "M";
        default:    return QVariant();
        }
    } else if (role == Qt::SizeHintRole && orientation == Qt::Orientation::Horizontal) {
        return QSize(65, 20);
    }
    return QVariant();
}

Qt::ItemFlags CCTExperimentTable::flags ( const QModelIndex & index) const
{
    if (index.isValid()) {
        auto& dil = cct[index.row()];
        if (index.column() == FPCol && dil.result(Phases::FP) < 0) return Qt::NoItemFlags;
        if (index.column() == BCol  && dil.result(Phases::B)  < 0) return Qt::NoItemFlags;
        if (index.column() == MCol)                                return Qt::NoItemFlags;
    }
    return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
}

QVariant CCTExperimentTable::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    auto& dil = cct[index.row()];

    double ret;
    switch (index.column()) {
    case WcCol: ret = dil.Wc; break;
    case HVCol: ret = dil.HV; break;
    case FPCol: ret = dil.result(Phases::FP); break;
    case BCol:  ret = dil.result(Phases::B); break;
    case MCol:  ret = dil.result(Phases::M); break;
    default: return QVariant();
    }
    if (ret < 0)
        return 0;
    return ret;
}

bool CCTExperimentTable::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid() || role != Qt::EditRole)
        return false;

    bool ok = false;
    double newVal = value.toDouble(&ok);
    if (!ok || newVal < 0)
        return false;

    auto& dil = cct[index.row()];
    switch (index.column()) {
    case WcCol: dil.Wc = newVal; break;
    case HVCol: dil.HV = newVal; break;
    default:
        if (newVal >= 1)
            newVal = std::min(newVal, 100.) / 100.;
        switch (index.column()) {
        case FPCol: dil.results[toId(Phases::FP)] = newVal; break;
        case BCol:  dil.results[toId(Phases::B)]  = newVal; break;
        default:    return false;
        }
        dil.results[toId(Phases::M)] = 1 - std::max(0., dil.results[toId(Phases::FP)]) - std::max(0., dil.results[toId(Phases::B)]);
    }
    return true;
}


//-------------------------------------------------------------------------------------------------------
//                                           HVParamsTable
//-------------------------------------------------------------------------------------------------------

QVariant HVParamsTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Orientation::Vertical) {
        return QString(phaseNames[section]);
    }
    return QVariant();
}

QVariant HVParamsTable::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    return hv.HV[index.row()];
}

bool HVParamsTable::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid() || role != Qt::EditRole)
        return false;

    bool ok = false;
    double newVal = value.toDouble(&ok);
    if (!ok || newVal < 0)
        return false;

    hv.HV[index.row()] = newVal;

    return true;
}


//-------------------------------------------------------------------------------------------------------
//                                           StartsTable
//-------------------------------------------------------------------------------------------------------

QVariant StartsTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Orientation::Vertical) {
        return QString(startParamsNames[section]);
    }
    return QVariant();
}

QVariant StartsTable::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    return starts.params[index.row()];
}

bool StartsTable::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid() || role != Qt::EditRole)
        return false;

    bool ok = false;
    double newVal = value.toDouble(&ok);
    if (!ok || newVal < 0)
        return false;

    starts.params[index.row()] = newVal;

    starts.params[toId(StartParameters::BT0)] = std::max(starts.param(StartParameters::BT0), starts.param(StartParameters::MT0));
    starts.params[toId(StartParameters::FPT0)] = std::max(starts.param(StartParameters::FPT0), starts.param(StartParameters::BT0));

    return true;
}


//-------------------------------------------------------------------------------------------------------
//                                           NsTable
//-------------------------------------------------------------------------------------------------------

QVariant AvramiParamsTable::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Orientation::Vertical) {
        return QString(avramiParamsNames[toId(AllAvramiParams[section])]);
    }
    return QVariant();
}

QVariant AvramiParamsTable::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    return avp.params[index.row()];
}

bool AvramiParamsTable::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid() || role != Qt::EditRole)
        return false;

    bool ok = false;
    double newVal = value.toDouble(&ok);
    if (!ok || newVal < 0)
        return false;

    avp.params[index.row()] = newVal;

    return true;
}
