#include "sstdoc.h"

#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>

#include <exception>

EngaugedCCT::EngaugedDil createDilatogramExperiment(std::vector<QPointF> const& engaugedPonts)
{
    if(engaugedPonts.size() > toId(Phases::phaseCount))
        throw std::runtime_error("Too much points for one experiment");
    EngaugedCCT::EngaugedDil dil;

    for (size_t i = 0; i < engaugedPonts.size(); i++)
        dil.points[toId(Phases::phaseCount) - engaugedPonts.size() + i] = engaugedPonts[i];

    return dil;
}

//--------------------------------------------------------------------------------------
void EngaugedCCT::loadSplittedByExperiment(std::string filename)
{
    std::ifstream file(filename);

    std::vector<QPointF> engaugedPonts;
    std::string str;
    while(std::getline(file, str)) {
        std::istringstream ststr(str);
        double x, y;
        ststr >> x >> y;

        if (ststr.fail()) {
            if (!engaugedPonts.empty()) {
                experiments.push_back(createDilatogramExperiment(engaugedPonts));
                engaugedPonts.clear();
            }
            ststr.clear();
        } else {
            engaugedPonts.emplace_back(x, y);
        }
    }

    if (!engaugedPonts.empty())
        experiments.push_back(createDilatogramExperiment(engaugedPonts));
}

void EngaugedCCT::loadSplittedByPhase(std::string filename)
{
    std::ifstream file(filename);

    std::array<std::vector<QPointF>, toId(Phases::phaseCount)> engaugedPonts;

    for (auto& phasePoints : engaugedPonts)
    {
        while (phasePoints.size() == 0 && !file.eof()) {
            std::string str;
            while(std::getline(file, str)) {
                std::istringstream ststr(str);
                double x, y;
                ststr >> x  >> y;

                if (ststr.fail())
                    break;

                phasePoints.emplace_back(x, y);
            }
        }
    }

    if(engaugedPonts[toId(Phases::FP)].size() > engaugedPonts[toId(Phases::B)].size() || engaugedPonts[toId(Phases::B)].size() > engaugedPonts[toId(Phases::M)].size())
        throw std::runtime_error("All FP points must have corresponding B and M points, all B points must have corresponding M points");

    size_t Monly = 0;
    for (; Monly < engaugedPonts[toId(Phases::M)].size() - engaugedPonts[toId(Phases::B)].size(); ++Monly)
        experiments.push_back(createDilatogramExperiment({engaugedPonts[toId(Phases::M)][Monly]}));

    size_t MBonly = 0;
    for (; MBonly < engaugedPonts[toId(Phases::B)].size() - engaugedPonts[toId(Phases::FP)].size(); ++MBonly)
        experiments.push_back(createDilatogramExperiment({engaugedPonts[toId(Phases::B)][MBonly], engaugedPonts[toId(Phases::M)][Monly + MBonly]}));

    for (size_t FPBM = 0; FPBM < engaugedPonts[toId(Phases::FP)].size(); ++FPBM)
        experiments.push_back(createDilatogramExperiment({engaugedPonts[toId(Phases::FP)][FPBM], engaugedPonts[toId(Phases::B)][MBonly + FPBM], engaugedPonts[toId(Phases::M)][Monly + MBonly + FPBM]}));
}

void EngaugedCCT::guessDilParams(double Tstart, bool logarigthmicCooling)
{
    T0 = Tstart;
    WcLog = logarigthmicCooling;
    experimentParams.guessDilParams(experiments, T0, WcLog);
}

void EngaugedCCT::CCTParameters::guessDilParams(std::vector<EngaugedDil> const& dils, double T0, bool WcLog)
{
    parameters.reserve(dils.size());
    for (auto& dil : dils) {
        DilParameters dilParms;

        for (auto p : AllPhases)
            if (!dil.isRealPoint(dil.point(p)))
                dilParms.results[toId(p)] = -1;

        if (!WcLog && T0 > 0) {
            double averageSpeed = 0;
            int phaseInDil = 0;
            for (auto p : dil.points)
            {
                if (dil.isRealPoint(p)){
                    if(T0 <= p.y())
                        throw std::runtime_error("Temperature of point is bigger than start temperature");
                    averageSpeed += (T0 - p.y()) / p.x();
                    ++phaseInDil;
                }
            }
            averageSpeed /= phaseInDil;

            dilParms.Wc = std::round(averageSpeed);
        }
        parameters.push_back(dilParms);
    }
}

std::vector<QPointF> EngaugedCCT::getPoints(Phases phase) const
{
    std::vector<QPointF> points;
    for (auto& dil : experiments) {
        if (EngaugedDil::isRealPoint(dil.point(phase)))
            points.push_back(dil.point(phase));
    }
    return points;
}

std::vector<QPointF> EngaugedCCT::getWcPoints(Phases phase) const
{
    std::vector<QPointF> points;
    if (experiments.size() != experimentParams.size())
        return points;

    for (size_t i = 0; i < experiments.size(); i++) {
        if (EngaugedDil::isRealPoint(experiments[i].point(phase)))
            points.emplace_back(experimentParams[i].Wc, experiments[i].point(phase).y());
    }
    return points;
}

std::vector<QPointF> EngaugedCCT::getWcRelativePointsSimplified(Phases phase) const
{
    std::vector<QPointF> points;
    for (auto & params : experimentParams.parameters) {
        double p = params.resultsSimple[toId(phase)];
        if (p > 0) {
            double possible = 1;
            if (phase == Phases::B || phase == Phases::M)
                possible -= params.resultsSimple[toId(Phases::FP)];
            if (phase == Phases::M)
                possible -= params.resultsSimple[toId(Phases::B)];
            points.emplace_back(params.Wc, p / possible);
        }
    }
    return points;
}

std::vector<QLineF> EngaugedCCT::getAllWc() const
{
    std::vector<QLineF> lines;
    if (!WcLog) {
        for (auto& dil : experimentParams) {
            if (!std::isnan(dil.Wc) && T0 > 0)
                lines.emplace_back(0, T0, T0 / dil.Wc, 0);
        }
    }
    return lines;
}

std::vector<QPointF> EngaugedCCT::CCTParameters::getHV2BPoints() const
{
    std::vector<QPointF> points;
    for (auto& dil : parameters) {
        points.emplace_back(dil.results[toId(Phases::B)], dil.HV);
    }
    return points;
}

bool EngaugedCCT::setDilParams(CCTParameters const& params)
{
    if (experiments.size() != params.size())
        return false;
    for (auto& param : params) {
        if (param.Wc <= 0)
            return false;
        if (param.HV <= 0)
            return false;
        double phaseSum = 0;
        for (auto p : AllPhases) {
            auto phaseRes = param.results[toId(p)];
            if (phaseRes != -1) {
                if (phaseRes < 0 || phaseRes > 1)
                    return false;
                phaseSum += phaseRes;
            }
        }
        if (phaseSum <= 0.9999 || phaseSum >= 1.0001)
            return false;
    }
    experimentParams = params;
    return true;
}

void EngaugedCCT::updateSimplifiedResults(HVData const& hvParams)
{
    for (auto& param : experimentParams.parameters) {
        hvParams.simplifiedPs(param.HV, param.resultsSimple);
    }
}

void EngaugedCCT::EngaugedDil::write(std::ofstream& fileStream)
{
    for (Phases p : AllPhases)
        fileStream << points[toId(p)].x() << ' ';
    fileStream << std::endl;
    for (Phases p : AllPhases)
        fileStream << points[toId(p)].y() << ' ';
    fileStream << std::endl;
}

void EngaugedCCT::EngaugedDil::read(std::ifstream& fileStream)
{
    for (Phases p : AllPhases)
        fileStream >> points[toId(p)].rx();
    for (Phases p : AllPhases)
        fileStream >> points[toId(p)].ry();
}

void EngaugedCCT::CCTParameters::writeOne(std::ofstream& fileStream, size_t i)
{
    assert(i < parameters.size());
    for (Phases p : AllPhases)
        fileStream << parameters[i].results[toId(p)] << ' ';
    fileStream << std::endl;
    for (Phases p : AllPhases)
        fileStream << parameters[i].resultsSimple[toId(p)] << ' ';
    fileStream << std::endl;
    fileStream << parameters[i].HV << ' ' << parameters[i].Wc;
    fileStream << std::endl;
}

void EngaugedCCT::CCTParameters::readOne(std::ifstream& fileStream)
{
    parameters.emplace_back();
    for (Phases p : AllPhases)
        fileStream >> parameters.back().results[toId(p)];
    for (Phases p : AllPhases)
        fileStream >> parameters.back().resultsSimple[toId(p)];
    fileStream >> parameters.back().HV >> parameters.back().Wc;
}

void EngaugedCCT::write(std::ofstream& fileStream)
{
    assert(experimentParams.size() == experiments.size());

    fileStream << T0 << ' ' << WcLog << ' ' << experiments.size() << std::endl;

    for (size_t i = 0; i < experiments.size(); ++i) {
        fileStream << std::endl;
        experiments[i].write(fileStream);
        experimentParams.writeOne(fileStream, i);
    }
    fileStream << std::endl;
}

void EngaugedCCT::read(std::ifstream& fileStream)
{
    size_t experimentsSize;
    fileStream >> T0 >> WcLog >> experimentsSize;

    experiments.clear();
    experimentParams.clear();
    experiments.reserve(experimentsSize);
    experimentParams.reserve(experimentsSize);

    for (size_t i = 0; i < experimentsSize; ++i) {
        experiments.emplace_back();
        experiments.back().read(fileStream);
        experimentParams.readOne(fileStream);
    }
}

//------------------------------------------------------------------------------------------------
void HVData::guessHVs(EngaugedCCT::CCTParameters const& params)
{
    QPointF maxHV{-1, 0};
    QPointF minHV{-1, std::numeric_limits<int>::max()};
    QPointF maxB {0, -1};
    for (auto& p : params) {
        if (maxHV.y() < p.HV)               maxHV = QPointF(p.result(Phases::B), p.HV);
        if (minHV.y() > p.HV)               minHV = QPointF(p.result(Phases::B), p.HV);
        if (maxB.x()  < p.result(Phases::B)) maxB = QPointF(p.result(Phases::B), p.HV);
    }
    if (maxHV.y() > maxB.y() && maxB.y() > minHV.y() &&
            maxHV.x() < 1. && maxB.x() <= 1. && minHV.x() < 1.) {
        HV[toId(Phases::B)]  = static_cast<int>(maxB.y());
        HV[toId(Phases::FP)] = static_cast<int>(maxB.y() - (maxB.y() - minHV.y()) / (1 - minHV.x()));
        HV[toId(Phases::M)]  = static_cast<int>(maxB.y() + (maxHV.y() - maxB.y()) / (1 - maxHV.x()));
    } else {
        HV[toId(Phases::FP)] = 270;
        HV[toId(Phases::B)]  = 300;
        HV[toId(Phases::M)]  = 400;
    }
}

void HVData::simplifiedPs(int hv, std::array<double, toId(Phases::phaseCount)>& out) const
{
    hv = std::min(std::max(HV[toId(Phases::FP)], hv), HV[toId(Phases::M)]);

    out = {0,0,0};
    if (hv < HV[toId(Phases::B)]) {
        out[toId(Phases::B)] = (hv - HV[toId(Phases::FP)]) / static_cast<double>(HV[toId(Phases::B)] - HV[toId(Phases::FP)]);
        out[toId(Phases::FP)] = 1 - out[toId(Phases::B)];
    } else {
        out[toId(Phases::M)] = (hv - HV[toId(Phases::B)]) / static_cast<double>(HV[toId(Phases::M)] - HV[toId(Phases::B)]);
        out[toId(Phases::B)] = 1 - out[toId(Phases::M)];
    }
}

//------------------------------------------------------------------------------------------------
void StartsData::guessStarts(EngaugedCCT const& cct)
{
    QPointF maxT0FP = QPointF(-1, -1);
    QPointF maxWcFP = QPointF(-1, -1);
    for(auto p : cct.getWcPoints(Phases::FP)) {
        if (p.y() > maxT0FP.y())
            maxT0FP = p;
        if (p.x() > maxWcFP.x())
            maxWcFP = p;
    }
    params[toId(StartParameters::FPts)] = (maxT0FP.y() - maxWcFP.y()) / (maxWcFP.x() - maxT0FP.x());
    params[toId(StartParameters::FPT0)] = maxWcFP.y() + param(StartParameters::FPts) * maxWcFP.x();

    QPointF maxT0B = QPointF(-1, -1);
    QPointF maxWcB = QPointF(-1, -1);
    for(auto p : cct.getWcPoints(Phases::B)) {
        if (p.y() > maxT0B.y())
            maxT0B = p;
        if (p.x() > maxWcB.x())
            maxWcB = p;
    }
    params[toId(StartParameters::Bts)] = (maxT0B.y() - maxWcB.y()) / (maxWcB.x() - maxT0B.x());
    params[toId(StartParameters::BT0)] = maxWcB.y() + param(StartParameters::Bts) * maxWcB.x();

    QPointF maxWcM = QPointF(-1, -1);
    for(auto p : cct.getWcPoints(Phases::M))
        if (p.x() > maxWcM.x())
            maxWcM = p;
    params[toId(StartParameters::MT0)] = maxWcM.y();
}

//------------------------------------------------------------------------------------------------
void AvramiData::guessAvramiParams(EngaugedCCT const& /*cct*/, StartsData const& /*starts*/)
{
}

//------------------------------------------------------------------------------------------------

RecalculatedResults::RecalcResult::RecalcResult(double wc, bool wcLog, StartsData const& startsParams, int hv, HVData const& hvParams) : Wc(wc), HV(hv)
{
    assert(!wcLog); //TODO
    assert(hvParams(Phases::FP) < hvParams(Phases::B) && hvParams(Phases::B) < hvParams(Phases::M));
    if (hv <= hvParams(Phases::FP)) {
        effectiveResults[toId(Phases::FP)] = 1;
    } else if (hv <= hvParams(Phases::B)) {
        effectiveResults[toId(Phases::B)] = (hv - hvParams(Phases::FP)) / (hvParams(Phases::B) - hvParams(Phases::FP));
        effectiveResults[toId(Phases::FP)] = 1 - effectiveResults[toId(Phases::B)];
    } else if (hv <= hvParams(Phases::M)) {
        effectiveResults[toId(Phases::M)] = (hv - hvParams(Phases::B)) / (hvParams(Phases::M) - hvParams(Phases::B));
        effectiveResults[toId(Phases::B)] = 1 - effectiveResults[toId(Phases::M)];
    } else {
        effectiveResults[toId(Phases::M)] = 1;
    }

    transformationTimes[toId(Phases::FP)] = (startsParams.param(StartParameters::FPT0) - startsParams.param(StartParameters::BT0)) / Wc;
    transformationTimes[toId(Phases::B)] = (startsParams.param(StartParameters::BT0) - startsParams.param(StartParameters::MT0)) / Wc;
    transformationTimes[toId(Phases::M)] = startsParams.param(StartParameters::MT0) / Wc;
}

void RecalculatedResults::RecalcResult::write(std::ofstream& fileStream)
{
    fileStream << Wc << ' ' << HV << std::endl;
    for (double  c : effectiveResults)
        fileStream << c << ' ';
    fileStream << std::endl;
    for (double  c : transformationTimes)
        fileStream << c << ' ';
    fileStream << std::endl;
}
void RecalculatedResults::RecalcResult::read(std::ifstream& fileStream)
{
    fileStream >> Wc >> HV;
    for (double& c : effectiveResults)
        fileStream >> c;
    for (double& c : transformationTimes)
        fileStream >> c;
}
//------------------------------------------------------------------------------------------------

void RecalculatedResults::write(std::ofstream& fileStream)
{
    fileStream << results.size() << std::endl;

    for (auto& r : results) {
        r.write(fileStream);
        fileStream << std::endl;
    }
    fileStream << std::endl;
}

void RecalculatedResults::read(std::ifstream& fileStream)
{
    size_t resultsSize;
    fileStream >> resultsSize;
    for (size_t i = 0; i < resultsSize; i++)
    {
        RecalcResult r;
        r.read(fileStream);
        results.push_back(r);
    }
}
//------------------------------------------------------------------------------------------------

SSTDoc::SSTDoc()
{

}

void SSTDoc::writeToFile(std::string const& filename)
{
    std::ofstream fileStream(filename);
    if (fileStream) {
        fileStream << std::setprecision(8);

        steel.write(fileStream);
        cct.write(fileStream);
        hv.write(fileStream);
        starts.write(fileStream);
        avp.write(fileStream);
    }
}

void SSTDoc::readFromFile(std::string const& filename)
{
    std::ifstream fileStream(filename);
    if (fileStream) {
        steel.read(fileStream);
        cct.read(fileStream);
        hv.read(fileStream);
        starts.read(fileStream);
        avp.read(fileStream);
    }
}

bool SSTDoc::loadEngauged(std::string filename, bool byPhase, double Tstart, bool logarigthmicCooling)
{
    try {
        cct = EngaugedCCT();
        if (byPhase) {
            cct.loadSplittedByPhase(filename);
        } else {
            cct.loadSplittedByExperiment(filename);
        }
        if (cct.experiments.empty()) {
            return false;
        }
        cct.guessDilParams(Tstart, logarigthmicCooling);
    } catch(std::runtime_error&) {
        return false;
    }

    return true;
}

bool SSTDoc::setDilParams(EngaugedCCT::CCTParameters const& params)
{
    if (cct.setDilParams(params)) {
        hv.guessHVs(cct.experimentParams);
        return true;
    } else {
        return false;
    }
}
