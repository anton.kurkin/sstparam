FORMS     = forms/window.ui
HEADERS   = displaywidget.h \
            window.h \
    diagdraw.h \
    sstdoc.h \
    tableview.h
RESOURCES =
SOURCES   = displaywidget.cpp \
            main.cpp \
            window.cpp \
    diagdraw.cpp \
    sstdoc.cpp \
    tableview.cpp

QT += widgets svg

CONFIG += c++14

INCLUDEPATH += $$PWD

# install
target.path = $$[QT_INSTALL_EXAMPLES]/svg/svggenerator
INSTALLS += target
