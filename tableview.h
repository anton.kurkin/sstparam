#ifndef TABLEVIEW_H
#define TABLEVIEW_H

#include <QtGui>
#include "sstdoc.h"

class SteelChemTable : public QAbstractTableModel
{
public:
    SteelChemTable() : chemData{} {}

    Qt::ItemFlags flags ( const QModelIndex &) const override { return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled; }
    int rowCount(const QModelIndex&)           const override { return toId(SteelChem::chemCount); }
    int columnCount(const QModelIndex&)        const override { return 1; }

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role) override;

    ChemArray const& getChem() const { return chemData; }
private:
    ChemArray chemData;
};


class CCTExperimentTable : public QAbstractTableModel
{
public:
    CCTExperimentTable(EngaugedCCT::CCTParameters const& CCTParams) : cct(CCTParams) {}

    int rowCount(const QModelIndex&)    const override { return cct.size(); }
    int columnCount(const QModelIndex&) const override { return colCount; }

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    Qt::ItemFlags flags ( const QModelIndex &) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    EngaugedCCT::CCTParameters const& getCCTParams() const { return cct; }
private:
    enum Columns {WcCol, HVCol, FPCol, BCol, MCol, colCount};
    EngaugedCCT::CCTParameters cct;
};


class HVParamsTable : public QAbstractTableModel
{
public:
    HVParamsTable(HVData const& HVParams) : hv(HVParams) {}

    Qt::ItemFlags flags ( const QModelIndex &) const override { return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled; }
    int rowCount(const QModelIndex&)    const override { return AllPhases.size(); }
    int columnCount(const QModelIndex&) const override { return 1; }

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    HVData const& getHVParams() const { return hv; }
private:
    HVData hv;
};

class StartsTable : public QAbstractTableModel
{
public:
    StartsTable(StartsData const& StartParams) : starts(StartParams) {}

    Qt::ItemFlags flags ( const QModelIndex &) const override { return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled; }
    int rowCount(const QModelIndex&)    const override { return AllStartParams.size(); }
    int columnCount(const QModelIndex&) const override { return 1; }

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    StartsData const& getStarts() const { return starts; }
private:
    StartsData starts;
};


class AvramiParamsTable : public QAbstractTableModel
{
public:
    AvramiParamsTable(AvramiData const& avramiParams) : avp(avramiParams) {}

    Qt::ItemFlags flags ( const QModelIndex &) const override { return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled; }
    int rowCount(const QModelIndex&)    const override { return AllAvramiParams.size(); }
    int columnCount(const QModelIndex&) const override { return 1; }

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    AvramiData const& getAvramiParams() const { return avp; }
private:
    AvramiData avp;
};

#endif // TABLEVIEW_H
