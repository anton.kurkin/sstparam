#ifndef WINDOW_H
#define WINDOW_H

#include <memory>

#include "ui_window.h"
#include "sstdoc.h"
#include "diagdraw.h"
#include "tableview.h"

class Window : public QWidget, private Ui::Window
{
    Q_OBJECT

public:
    Window(QWidget *parent = nullptr);

public slots:
    void saveSvg();

private slots:
    void on_openButton_clicked();
    void on_applyCCTButton_clicked();
    void on_CCTDoneButton_clicked();

    void on_applyHVButton_clicked();
    void on_HVDoneButton_clicked();

    void on_saveDocumentButton_clicked();
    void on_openDocumentButton_clicked();

    void on_startsDoneButton_clicked();

    void on_applyStartsButton_clicked();

    void on_applyAvramiButton_clicked();

private:
    QString path;
    std::unique_ptr<SSTDoc> doc;

    SteelChemTable chem;
    std::unique_ptr<CCTExperimentTable> cctTable;
    std::unique_ptr<HVParamsTable> hvTable;
    std::unique_ptr<StartsTable> startsTable;
    std::unique_ptr<AvramiParamsTable> avramiTable;
    void updateCCTTable(EngaugedCCT::CCTParameters const& CCTParams)    { cctTable = std::make_unique<CCTExperimentTable>(CCTParams);   CCTParamsTable->setModel(cctTable.get());   CCTParamsTable->resizeColumnsToContents(); }
    void updateHVTable(HVData const& hv)                                { hvTable = std::make_unique<HVParamsTable>(hv);                HVTable->setModel(hvTable.get()); }
    void updateStartsTable(StartsData const& starts)                    { startsTable = std::make_unique<StartsTable>(starts);          startParamsTable->setModel(startsTable.get()); }
    void updateAvramiTable(AvramiData const& avp)                       { avramiTable = std::make_unique<AvramiParamsTable>(avp);       avramiParamsTable->setModel(avramiTable.get()); }

    std::shared_ptr<DiagDraw> diag;
    void updateCCTDraw(EngaugedCCT const& CCT)                                              { diag = std::make_shared<CCTDiagDraw>(CCT);                    displayWidget->setDiagDraw(diag); }
    void updateHVDraw(EngaugedCCT::CCTParameters const& CCTParams, HVData const& HVParams)  { diag = std::make_shared<HVSelectDraw>(CCTParams, HVParams);   displayWidget->setDiagDraw(diag); }
    void updateStartTimeDraw(EngaugedCCT const& CCT, StartsData const& startParams)         { diag = std::make_shared<StartTimeDraw>(CCT, startParams);     displayWidget->setDiagDraw(diag); }
    void updateAvramiParamsDraw(EngaugedCCT const& CCT, AvramiData const& avramiParams)     { diag = std::make_shared<AvramiParamsDraw>(CCT, avramiParams); displayWidget->setDiagDraw(diag); }
};

#endif
