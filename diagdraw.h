#ifndef DIAGDRAW_H
#define DIAGDRAW_H

#include <QPainter>
#include <QPainterPath>
#include <memory>

#include "sstdoc.h"

class DiagDraw
{
public:
    DiagDraw();

    void paint(QRect const& drawArea, QPainter &painter);

    inline QPointF getPoint(QPoint p);

    QRectF getCoordRect() { return coordRect; }

protected:
    void setCoordRect(QRectF newCoordRect, bool logX, bool logY);

    void cachePoint(QPointF p);
    void cacheLine(QLineF l);
    void cacheLinePlain(QLineF l);

    QRectF viewCoordRect() { return coordRect; }

    bool xLog = false;
    bool yLog = false;

private:
    struct CoordLines;

    void drawPoint(QPainter &painter, QPointF p);
    void drawLine(QPainter &painter, QLineF l);
    void drawLineDirect(QPainter &painter, QLineF lf);

    QLineF clampLine(QLineF lf);
    QLineF cutLineSinglePoint(QLineF l); //p1 inside, p2 outside
    bool cutLineBothPoints(QLineF& l); //p1 outside, p2 outside, probably no intersection
    QPoint transformPoint(QPointF p);
    std::vector<QPoint> transformLine(QLineF l);
    CoordLines calcCoordLines(double min, double max, bool log, int /*width*/ = 0);

    QRectF coordRect;
    std::array<QLineF, 4> coordRectLines;

    QRect  diagramArea;

    struct CoordLines { std::vector<double> coords; std::vector<double> coordTexts; };
    CoordLines clX, clY;

    std::vector<QPointF> points;
    std::vector<QLineF>  lines;
    std::vector<QLineF>  linesPlain;

    QPainterPath pointShape;
};

class CCTDiagDraw : public DiagDraw
{
public:
    CCTDiagDraw(EngaugedCCT const& CCTDiag);

    void setCoordRect(QRectF newCoordRect);

private:
    EngaugedCCT const cct;
};

class HVSelectDraw : public DiagDraw
{
public:
    HVSelectDraw(EngaugedCCT::CCTParameters const& CCTParams, HVData const& HVParams);

    void setCoordRect(QRectF newCoordRect);

private:
    EngaugedCCT::CCTParameters const cct;
    HVData const hv;
};

class StartTimeDraw : public DiagDraw
{
public:
    StartTimeDraw(EngaugedCCT const& CCT, StartsData const& st);

    void setCoordRect(QRectF newCoordRect);

private:
    EngaugedCCT const cct;
    StartsData const starts;
};

class AvramiParamsDraw : public DiagDraw
{
public:
    AvramiParamsDraw(EngaugedCCT const& CCT, AvramiData const& avramiParams);

    void setCoordRect(QRectF newCoordRect);

private:
    EngaugedCCT const cct;
    AvramiData const avp;
};

#endif // DIAGDRAW_H
