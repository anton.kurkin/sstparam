#ifndef DISPLAYWIDGET_H
#define DISPLAYWIDGET_H

#include <QColor>
#include <QWidget>
#include <QPainter>
#include <vector>
#include <iostream>
#include <memory>

#include "diagdraw.h"

class DisplayWidget : public QWidget
{
    Q_OBJECT

    std::weak_ptr<DiagDraw> diag;
public:
    DisplayWidget(QWidget *parent = 0);
    void setDiagDraw(std::weak_ptr<DiagDraw> d) { diag = std::move(d); update(); }

    void paint(QRect const& drawArea, QPainter &painter);

protected:
    void paintEvent(QPaintEvent *event) override;

};

#endif
