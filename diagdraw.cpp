#include "diagdraw.h"

void drawText(QPainter & painter, qreal x, qreal y, Qt::Alignment flags,
              const QString & text, QRectF * boundingRect = nullptr)
{
   const qreal size = 32767.0;
   QPointF corner(x, y - size);
   if (flags & Qt::AlignHCenter) corner.rx() -= size/2.0;
   else if (flags & Qt::AlignRight) corner.rx() -= size;
   if (flags & Qt::AlignVCenter) corner.ry() += size/2.0;
   else if (flags & Qt::AlignTop) corner.ry() += size;
   else flags |= Qt::AlignBottom;
   QRectF rect{corner.x(), corner.y(), size, size};
   painter.drawText(rect, flags, text, boundingRect);
}

void drawText(QPainter & painter, const QPointF & point, Qt::Alignment flags,
              const QString & text, QRectF * boundingRect = {})
{
   drawText(painter, point.x(), point.y(), flags, text, boundingRect);
}

DiagDraw::DiagDraw()
{
    pointShape.addEllipse(QPoint{0,0}, 3, 3);
}

DiagDraw::CoordLines DiagDraw::calcCoordLines(double min, double max, bool log, int /*width*/)
{
    if (min > max)
        std::swap(min, max);
    CoordLines cl;
    if (!log) {
        double step = std::pow(10, std::round(std::log10(max - min) - 1));
        double start = std::ceil(min / step) * step;
        for (double d = start; d <= max; d += step) {
            cl.coords.push_back(d);
            cl.coordTexts.push_back(d);
        }
    } else {
        double step = std::pow(10, std::floor(std::log10(min)));
        cl.coordTexts.push_back(step);
        double start = std::ceil(min / step) * step;
        for (double d = start; d <= max; d += step) {
            if (step != cl.coordTexts.back())
                cl.coordTexts.push_back(step);
            step = std::pow(10, std::floor(std::log10(d)));
            cl.coords.push_back(d);
        }
    }
    return cl;
}

void DiagDraw::paint(QRect const& drawArea, QPainter &painter)
{
    int LeftSpace = 50;
    int DownSpace = 50;
    int RightSpace = 5;
    int UpSpace = 5;
    diagramArea = QRect(drawArea.x() + LeftSpace, drawArea.y() + UpSpace, drawArea.width() - LeftSpace - RightSpace, drawArea.height() - UpSpace - DownSpace);

    painter.setClipRect(drawArea);
    painter.fillRect(drawArea, Qt::white);
    if (coordRect.isEmpty())
        return;

    painter.setPen(Qt::lightGray);
    for (auto& c : clX.coords)
        drawLineDirect(painter, QLineF(c, coordRect.bottom(), c, coordRect.top()));
    painter.setPen(Qt::black);
    for (auto& c : clX.coordTexts)
        drawText(painter, transformPoint({c, coordRect.top()}), Qt::AlignTop | Qt::AlignHCenter, QString::number(c));

    painter.setPen(Qt::lightGray);
    for (auto& c : clY.coords)
        drawLineDirect(painter, QLineF(coordRect.left(), c, coordRect.right(), c));
    painter.setPen(Qt::black);
    for (auto& c : clY.coordTexts)
        drawText(painter, transformPoint({coordRect.left(), c}), Qt::AlignVCenter | Qt::AlignRight, QString::number(c));

    painter.drawRect(diagramArea);

    for (auto& p : points)
        drawPoint(painter, p);

    for (auto& l : lines)
        drawLine(painter, l);

    for (auto& l : linesPlain)
        drawLineDirect(painter, l);
}

void DiagDraw::setCoordRect(QRectF c, bool logX, bool logY)
{
    const double logCoordMin = 0.01;

    xLog = logX;
    yLog = logY;

    coordRect = QRectF(xLog ? std::max(logCoordMin, c.left()) : c.left(),
                       yLog ? std::max(logCoordMin, c.top()) : c.top(),
                       c.width(),
                       c.height());

    coordRectLines = {QLineF(coordRect.topLeft(),     coordRect.bottomLeft()),
                      QLineF(coordRect.bottomLeft(),  coordRect.bottomRight()),
                      QLineF(coordRect.bottomRight(), coordRect.topRight()),
                      QLineF(coordRect.topRight(),    coordRect.topLeft())};

    clX = calcCoordLines(coordRect.left(), coordRect.right(), xLog);
    clY = calcCoordLines(coordRect.bottom(), coordRect.top(), yLog);

    points.clear();
    lines.clear();
}

QLineF DiagDraw::cutLineSinglePoint(QLineF l)
{
    QPointF sect;
    for (QLineF sl : coordRectLines)
        if (l.intersects(sl, &sect) == QLineF::BoundedIntersection &&
                sect != l.p1())
            return QLineF(l.p1(), sect);
    //assert(false);
    return QLineF(l);
}

bool DiagDraw::cutLineBothPoints(QLineF& l)
{
    std::array<QPointF, 2> ps;

    size_t i = 0;
    for (auto coordLine : coordRectLines) {
        if (l.intersects(coordLine, &ps[i]) == QLineF::BoundedIntersection) {
            if (i == 0 || ps[0] != ps[i]) { //first point or not equal to first point
                ++i;
                if (i == ps.size())
                {
                    l = {ps[0], ps[1]};
                    return true;
                }
            }
        }
    }
    return false;
}

void DiagDraw::cachePoint(QPointF pf)
{
    if (!coordRect.contains(pf))
        return;

    points.push_back(pf);
}

QLineF DiagDraw::clampLine(QLineF lf)
{
    if (!coordRect.contains(lf.p1()) && !coordRect.contains(lf.p2())) {
        if (cutLineBothPoints(lf))
            return lf;
    } else if (!coordRect.contains(lf.p2())) {
        return cutLineSinglePoint(lf);
    } else if (!coordRect.contains(lf.p1())) {
        return cutLineSinglePoint(QLineF(lf.p2(), lf.p1()));
    }
    return lf;
}

void DiagDraw::cacheLine(QLineF lf)
{
    lines.push_back(clampLine(lf));
}

void DiagDraw::cacheLinePlain(QLineF lf)
{
    linesPlain.push_back(clampLine(lf));
}

void DiagDraw::drawPoint(QPainter &painter, QPointF pf)
{
    auto p = transformPoint(pf);

    painter.translate(p.x(), p.y());
    painter.drawPath(pointShape);
    painter.translate(-p.x(), -p.y());
}

void DiagDraw::drawLine(QPainter &painter, QLineF lf)
{
    auto l = transformLine(lf);
    painter.drawPolyline(l.data(), l.size());
}

void DiagDraw::drawLineDirect(QPainter &painter, QLineF lf)
{
    painter.drawLine(transformPoint(lf.p1()), transformPoint(lf.p2()));
}

QPointF DiagDraw::getPoint(QPoint p)
{
    assert(!xLog && !yLog);

    p -= diagramArea.topLeft(); //Xmin, Ymin
    QPointF pf = QPointF(p.x() / (qreal)diagramArea.width(), p.y() / (qreal)diagramArea.height());

    pf = coordRect.topLeft() + QPointF(pf.x() * coordRect.width(), (1 - pf.y()) * coordRect.height());
    return pf;
}

inline double toLogIf(double min, double v, double max, bool log)
{
    assert(min > 0 || !log);
    assert(v > 0 || !log);
    assert(max > 0 || !log);

    if (log) {
        return (std::log(v) - std::log(min)) / (std::log(max) - std::log(min));
    } else {
        return (v - min) / (max - min);
    }
}

QPoint DiagDraw::transformPoint(QPointF pf)
{
    pf = QPointF(toLogIf(coordRect.left(), pf.x(), coordRect.right(), xLog),
                 toLogIf(coordRect.top(), pf.y(), coordRect.bottom(), yLog));

    QPoint p = diagramArea.topLeft() + QPoint(pf.x() * diagramArea.width(), (1 - pf.y()) * diagramArea.height());
    return p;
}

double logInterval(double a, double b) {return std::abs(std::log(a) - std::log(b));}

std::vector<QPoint> DiagDraw::transformLine(QLineF l)
{
    QPointF startP = l.p1();
    QPointF endP = l.p2();

    std::vector<QPoint> vec;

    const double logStep = 0.01;
    if (xLog && (!yLog || logInterval(startP.x(), endP.x()) >=
                          logInterval(startP.y(), endP.y()))) {

        if (startP.x() > endP.x())
            std::swap(startP, endP);
        vec.push_back(transformPoint(startP));

        double end = std::log(endP.x());
        for (double logX = std::log(startP.x()) + logStep; logX < end; logX += logStep) {
            QPointF p;
            auto result = l.intersects(QLineF(std::exp(logX), coordRect.top(), std::exp(logX), coordRect.bottom()), &p);
            if (result == QLineF::BoundedIntersection)
                vec.push_back(transformPoint(p));
        }
    } else if (yLog) {
        if (startP.y() > endP.y())
            std::swap(startP, endP);
        vec.push_back(transformPoint(startP));

        double end = std::log(endP.y());
        for (double logY = std::log(startP.y()) + logStep; logY < end; logY += logStep) {
            QPointF p;
            auto result = l.intersects(QLineF(coordRect.left(), std::exp(logY), coordRect.right(), std::exp(logY)), &p);
            if (result == QLineF::BoundedIntersection)
                vec.push_back(transformPoint(p));
        }
    } else {
        vec.push_back(transformPoint(startP));
    }

    vec.push_back(transformPoint(endP));

    return vec;
}

//---------------------------------------------------------------------------------------------------
CCTDiagDraw::CCTDiagDraw(EngaugedCCT const& CCTDiag) : cct(CCTDiag)
{
    double tMax = 0;
    for (auto phase : AllPhases)
        for (auto point : cct.getPoints(phase))
            tMax = std::max(tMax, point.x());

    setCoordRect(QRectF(1, 0, tMax, cct.T0));
}

void CCTDiagDraw::setCoordRect(QRectF newCoordRect)
{
    DiagDraw::setCoordRect(newCoordRect, true, false);

    for (auto line : cct.getAllWc())
        cacheLine(line);

    for (auto phase : AllPhases) {
        auto pts = cct.getPoints(phase);
        if (!pts.empty()) {
            cachePoint(pts.front());
            for (auto cur = pts.begin(), prev = cur++; cur != pts.end(); prev = cur++) {
                cachePoint(*cur);
                cacheLinePlain({*prev, *cur});
            }
        }
    }
}

//---------------------------------------------------------------------------------------------------
HVSelectDraw::HVSelectDraw(EngaugedCCT::CCTParameters const& CCTParams, HVData const& HVParams) : cct(CCTParams), hv(HVParams)
{
    double HVmin = 100;
    double HVmax = 450;

    for (auto point : cct.getHV2BPoints()) {
        HVmax = std::max(HVmax, point.y());
        HVmin = std::min(HVmin, point.y());
    }

    setCoordRect(QRectF(0, HVmin, 1, HVmax - HVmin));
}

void HVSelectDraw::setCoordRect(QRectF newCoordRect)
{
    DiagDraw::setCoordRect(newCoordRect, false, false);

    for (auto point : cct.getHV2BPoints())
        cachePoint(point);

    cacheLinePlain(QLineF(1, hv.HV[toId(Phases::B)], 0, hv.HV[toId(Phases::FP)]));
    cacheLinePlain(QLineF(1, hv.HV[toId(Phases::B)], 0, hv.HV[toId(Phases::M)]));
}

//---------------------------------------------------------------------------------------------------
StartTimeDraw::StartTimeDraw(EngaugedCCT const& CCT, StartsData const& st) : cct(CCT), starts(st)
{
    double WcMax = 0;
    for (auto& dil : cct.experimentParams)
        WcMax = std::max(WcMax, dil.Wc);

    setCoordRect(QRectF(0, 0, WcMax, cct.T0));
}

void StartTimeDraw::setCoordRect(QRectF newCoordRect)
{
    DiagDraw::setCoordRect(newCoordRect, false, false);

    for (auto phase : AllPhases)
        for (auto pt : cct.getWcPoints(phase))
            cachePoint(pt);

    QLineF MLine  = QLineF(0, starts.param(StartParameters::MT0),  newCoordRect.width(), starts.param(StartParameters::MT0));
    QLineF BLine  = QLineF(0, starts.param(StartParameters::BT0),  (starts.param(StartParameters::BT0)  - starts.param(StartParameters::MT0)) / starts.param(StartParameters::Bts),  starts.param(StartParameters::MT0));
    QLineF FPLine = QLineF(0, starts.param(StartParameters::FPT0), (starts.param(StartParameters::FPT0) - starts.param(StartParameters::BT0)) / starts.param(StartParameters::FPts), starts.param(StartParameters::BT0));

    cacheLine(FPLine);
    cacheLine(BLine);
    cacheLine(MLine);
}

//---------------------------------------------------------------------------------------------------
AvramiParamsDraw::AvramiParamsDraw(EngaugedCCT const& CCT, AvramiData const& avramiParams) : cct(CCT), avp(avramiParams)
{
    double WcMax = 0;
    for (auto& dil : cct.experimentParams)
        WcMax = std::max(WcMax, dil.Wc);

    setCoordRect(QRectF(1, 0.1, WcMax, 1.01));
}

void AvramiParamsDraw::setCoordRect(QRectF newCoordRect)
{
    DiagDraw::setCoordRect(newCoordRect, true, true);

    for (auto phase : {Phases::FP, Phases::B}) {
        auto points = cct.getWcRelativePointsSimplified(phase);
        if (points.size() > 1)
        {
            for (auto next = points.begin(), it = next++; next < points.end(); it = next++) {
                cachePoint(*it);
                cacheLine(QLineF(*it, *next));
            }
        }
        cachePoint(points.back());
    }

    auto cr = viewCoordRect();
    cacheLinePlain(QLineF(cr.left(), 0.5, cr.right(), 0.5));
    for (auto param : {AvramiParameters::FPwc50, AvramiParameters::Bwc50})
        cacheLinePlain(QLineF(avp(param), 0.1, avp(param), 0.5));

    for (auto param : {AvramiParameters::FPn, AvramiParameters::Bn}) {
        auto wc = avp(param == AvramiParameters::FPn ? AvramiParameters::FPwc50 : AvramiParameters::Bwc50);
        for (double p : {0.01, 0.99}) {
            double wp = std::exp(std::log(wc) - (std::log(p) - std::log(0.5)) / avp(param));
            cacheLinePlain(QLineF(wp, p, wc, 0.5));
        }
    }
}
