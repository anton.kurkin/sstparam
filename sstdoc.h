#ifndef SSTDOC_H
#define SSTDOC_H

#include <QRectF>
#include <QLineF>
#include <vector>
#include <array>
#include <string>
#include <limits>
#include <cmath>
#include <fstream>

enum class Phases {FP, B, M, phaseCount};
constexpr size_t toId(Phases p) noexcept { return static_cast<size_t>(p); }
const std::array<Phases, toId(Phases::phaseCount)> AllPhases = {Phases::FP, Phases::B, Phases::M};
const std::array<char const*, toId(Phases::phaseCount)> phaseNames = {"FP", "B", "M"};
constexpr char const* toStr(Phases p) noexcept { return phaseNames[toId(p)]; }

enum class SteelChem {C, Si, Mn, Cr, Ni, Mo, V, Nb, Al, Cu, Ti, W, chemCount};
constexpr size_t toId(SteelChem c) noexcept { return static_cast<size_t>(c); }
const std::array<SteelChem, toId(SteelChem::chemCount)> AllSteelChems = {SteelChem::C, SteelChem::Si, SteelChem::Mn, SteelChem::Cr, SteelChem::Ni, SteelChem::Mo, SteelChem::V, SteelChem::Nb, SteelChem::Al, SteelChem::Cu, SteelChem::Ti, SteelChem::W};
const std::array<char const*, toId(SteelChem::chemCount)> chemNames = {"C", "Si", "Mn", "Cr", "Ni", "Mo", "V", "Nb", "Al", "Cu", "Ti", "W"};
constexpr char const* toStr(SteelChem c) noexcept { return chemNames[toId(c)]; }

enum class StartParameters {FPT0, BT0, MT0, FPts, Bts, startsCount};
constexpr size_t toId(StartParameters c) noexcept { return static_cast<size_t>(c); }
const std::array<StartParameters, toId(StartParameters::startsCount)> AllStartParams = {StartParameters::FPT0, StartParameters::BT0, StartParameters::MT0, StartParameters::FPts, StartParameters::Bts};
const std::array<char const*, toId(StartParameters::startsCount)> startParamsNames = {"FPT0", "BT0", "MT0", "FPts", "Bts"};
constexpr char const* toStr(StartParameters c) noexcept { return startParamsNames[toId(c)]; }

enum class AvramiParameters {FPn, Bn, FPwc50, Bwc50, avramiCount};
constexpr size_t toId(AvramiParameters c) noexcept { return static_cast<size_t>(c); }
const std::array<AvramiParameters, toId(AvramiParameters::avramiCount)> AllAvramiParams = {AvramiParameters::FPn, AvramiParameters::Bn, AvramiParameters::FPwc50, AvramiParameters::Bwc50};
const std::array<char const*, toId(StartParameters::startsCount)> avramiParamsNames = {"FPn", "Bn", "FPwc50", "Bwc50"};
constexpr char const* toStr(AvramiParameters c) noexcept { return avramiParamsNames[toId(c)]; }

struct HVData;

struct EngaugedCCT
{
    void loadSplittedByExperiment(std::string filename);
    void loadSplittedByPhase(std::string filename);
    void guessDilParams(double T0, bool WcLog);

    std::vector<QPointF> getPoints(Phases phase) const;
    std::vector<QPointF> getWcPoints(Phases phase) const;
    std::vector<QPointF> getWcRelativePointsSimplified(Phases phase) const;
    std::vector<QLineF>  getAllWc() const;
    size_t getExperimentCount() { return experiments.size(); }

    struct EngaugedDil
    {
        static QPointF unrealPoint() {return QPointF(-1, -1);}
        static bool isRealPoint(QPointF p) {return p.x() >= 0;}

        QPointF point(Phases p) const {return points[toId(p)];}

        std::array<QPointF, toId(Phases::phaseCount)> points {{unrealPoint(), unrealPoint(), unrealPoint()}};

        void write(std::ofstream& fileStream);
        void read(std::ifstream& fileStream);
    };
    std::vector<EngaugedDil> experiments;

    struct CCTParameters
    {
        struct DilParameters
        {
            int HV = -1;
            std::array<double, toId(Phases::phaseCount)> results {{0, 0, 1}};
            std::array<double, toId(Phases::phaseCount)> resultsSimple {{0, 0, 0}};
            double Wc = -1;

            double result(Phases p) const { return results[toId(p)]; }
        };
        using ContType = std::vector<DilParameters>;
        ContType parameters;

        ContType::const_iterator begin() const { return parameters.begin(); }
        ContType::const_iterator end() const { return parameters.end(); }
        ContType::size_type size() const { return parameters.size(); }
        void clear() { parameters.clear(); }
        void reserve(ContType::size_type n) { parameters.reserve(n); }
        DilParameters const& operator[](ContType::size_type i) const { assert(i < size()); return parameters[i]; }
        DilParameters      & operator[](ContType::size_type i)       { assert(i < size()); return parameters[i]; }

        void guessDilParams(std::vector<EngaugedDil> const& dils, double Tstart, bool logarigthmicCooling);
        std::vector<QPointF> getHV2BPoints() const;

        void writeOne(std::ofstream& fileStream, size_t i);
        void readOne(std::ifstream& fileStream);
    };
    CCTParameters experimentParams;

    bool setDilParams(CCTParameters const& dilParams);
    void updateSimplifiedResults(HVData const& hvParams);

    double T0 = -1;
    bool WcLog = false;

    void write(std::ofstream& fileStream);
    void read(std::ifstream& fileStream);
};

using ChemArray = std::array<double, toId(SteelChem::chemCount)>;
struct SteelData
{
    std::string name;
    ChemArray chem;

    void write(std::ofstream& fileStream) { fileStream << name << std::endl; for (double  c : chem) fileStream << c << ' '; fileStream << std::endl; }
    void read(std::ifstream& fileStream)  { std::getline(fileStream, name); for (double& c : chem) fileStream >> c; }
};

struct HVData
{
    void guessHVs(EngaugedCCT::CCTParameters const& params);
    std::array<int, toId(Phases::phaseCount)> HV {{0,0,0}};

    double operator() (Phases p) const { return HV[toId(p)]; }

    void simplifiedPs(int hv, std::array<double, toId(Phases::phaseCount)>& out) const;

    void write(std::ofstream& fileStream) { for (int  c : HV) fileStream << c << ' '; fileStream << std::endl; }
    void read(std::ifstream& fileStream)  { for (int& c : HV) fileStream >> c; }
};

struct StartsData
{
    std::array<double, AllStartParams.size()> params {{0,0,0,0,0}};

    void guessStarts(EngaugedCCT const& cct);

    double param (StartParameters p) const { return params[toId(p)]; }

    void write(std::ofstream& fileStream) { for (double  c : params) fileStream << c << ' '; fileStream << std::endl; }
    void read(std::ifstream& fileStream)  { for (double& c : params) fileStream >> c; }
};

struct AvramiData
{
    std::array<double, AllAvramiParams.size()> params {{0,0}};

    void guessAvramiParams(EngaugedCCT const& cct, StartsData const& starts);

    double operator() (AvramiParameters c) const { return params[toId(c)]; }

    void write(std::ofstream& fileStream) { for (double  c : params) fileStream << c << ' '; fileStream << std::endl; }
    void read(std::ifstream& fileStream)  { for (double& c : params) fileStream >> c; }
};

struct RecalculatedResults
{
    struct RecalcResult
    {
        double Wc = -1;
        int HV = -1;

        std::array<double, AllPhases.size()> effectiveResults {{0,0}};
        std::array<double, AllPhases.size()> transformationTimes {{0,0}};
        QPointF Point (Phases p) const { return QPointF(transformationTimes[toId(p)], effectiveResults[toId(p)]); }

        RecalcResult(double wc, bool wcLog, StartsData const& startsParams, int hv, HVData const& hvParams);
        RecalcResult() {}

        void write(std::ofstream& fileStream);
        void read(std::ifstream& fileStream);
    };

    std::vector<RecalcResult> results;

    RecalculatedResults(EngaugedCCT const& cct, HVData const& hv, StartsData const& starts) { for (auto& e : cct.experimentParams) results.emplace_back(e.Wc, cct.WcLog, starts, e.HV, hv); }

    void write(std::ofstream& fileStream);
    void read(std::ifstream& fileStream);
};

class SSTDoc
{
public:
    SSTDoc();
    bool loadEngauged(std::string filename, bool byPhase, double Tstart, bool logarigthmicCooling);
    void setSteel(std::string&& name, ChemArray const& chem) { steel = SteelData{std::move(name), chem}; }

    EngaugedCCT const& getCCT() { return cct; }
    bool setDilParams(EngaugedCCT::CCTParameters const& params);

    HVData const& getHV() { return hv; }
    bool setHV(HVData const& HVDiag) { hv = HVDiag; cct.updateSimplifiedResults(hv); return true; }

    void guessStarts() { starts.guessStarts(cct); }
    StartsData const& getStarts() { return starts; }
    bool setStarts(StartsData const& s) { starts = s; return true; }

    void guessAvramiParams() { avp.guessAvramiParams(cct, starts); }
    AvramiData const& getAvramiParams() { return avp; }
    bool setAvramiParams(AvramiData const& a) { avp = a; return true; }

    void writeToFile(std::string const& filename);
    void readFromFile(std::string const& filename);
private:
    SteelData steel;
    EngaugedCCT cct;
    HVData hv;
    StartsData starts;
    AvramiData avp;
};

#endif // SSTDOC_H
