# SSTParam

SSTParam is a tool for processing digitized Continuous Cooling Transformation (CCT) Diagrams with a purpose to automatically generate the parameters of the transformation by the digitized points

## Contributing

Development is discontinued.

## License

[MIT](https://choosealicense.com/licenses/mit/)
