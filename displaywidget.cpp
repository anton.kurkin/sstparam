#include <QtWidgets>
#include "displaywidget.h"

DisplayWidget::DisplayWidget(QWidget *parent) :
    QWidget(parent)
{
}

void DisplayWidget::paintEvent(QPaintEvent * /* event */)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    paint(rect(), painter);
    painter.end();
}

void DisplayWidget::paint(QRect const& drawArea, QPainter &painter)
{
    if (auto lockedDiag = diag.lock()) {
        lockedDiag->paint(drawArea, painter);
    }
}
