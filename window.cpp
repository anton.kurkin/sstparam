/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QColorDialog>
#include <QFileDialog>
#include <QPainter>
#include <QSvgGenerator>
#include <QFileDialog>
#include "window.h"
#include "displaywidget.h"


Window::Window(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);

    //PanelSwitcher->tabBar()->hide();
    PanelSwitcher->setCurrentWidget(CCTLoadTab);

    doc = std::make_unique<SSTDoc>();
    chemTable->setModel(&chem);
}

void Window::saveSvg()
{
    QString newPath = QFileDialog::getSaveFileName(this, tr("Save SVG"),
        path, tr("SVG files (*.svg)"));

    if (newPath.isEmpty())
        return;

    path = newPath;
    QRect drawArea(0, 0, 500, 500);

    QSvgGenerator generator;
    generator.setFileName(path);
    generator.setSize(QSize(500, 500));
    generator.setViewBox(drawArea);

    QPainter painter;
    painter.begin(&generator);
    displayWidget->paint(drawArea, painter);
    painter.end();
}

void Window::on_openButton_clicked()
{
    QString path = QFileDialog::getOpenFileName();
    if (path.isEmpty())
        return;
    doc->setSteel(steelNameEdit->text().toStdString(), chem.getChem());
    if (doc->loadEngauged(path.toStdString(), phaseSortBox->isChecked(), T0SpinBox->value(), WcLogBox->isChecked())) {
        updateCCTDraw(doc->getCCT());
        updateCCTTable(doc->getCCT().experimentParams);

        PanelSwitcher->setCurrentWidget(CCTParamsTab);
    }
}

void Window::on_applyCCTButton_clicked()
{
    if (cctTable != nullptr)
        if (doc->setDilParams(cctTable->getCCTParams()))
            updateCCTDraw(doc->getCCT());
}

void Window::on_CCTDoneButton_clicked()
{
    if (cctTable != nullptr) {
        if (doc->setDilParams(cctTable->getCCTParams())) {
            updateHVDraw(doc->getCCT().experimentParams, doc->getHV());
            updateHVTable(doc->getHV());

            PanelSwitcher->setCurrentWidget(HVTab);
        }
    }
}

void Window::on_applyHVButton_clicked()
{
    if (hvTable != nullptr)
        if (doc->setHV(hvTable->getHVParams()))
            updateHVDraw(doc->getCCT().experimentParams, doc->getHV());
}

void Window::on_HVDoneButton_clicked()
{
    if (hvTable != nullptr) {
        if (doc->setHV(hvTable->getHVParams())) {
            doc->guessStarts();
            updateStartTimeDraw(doc->getCCT(), doc->getStarts());
            updateStartsTable(doc->getStarts());

            PanelSwitcher->setCurrentWidget(StartTab);
        }
    }
}

void Window::on_applyStartsButton_clicked()
{
    if (startsTable != nullptr)
        if (doc->setStarts(startsTable->getStarts()))
            updateStartTimeDraw(doc->getCCT(), doc->getStarts());
}

void Window::on_startsDoneButton_clicked()
{
    if (startsTable != nullptr) {
        if (doc->setStarts(startsTable->getStarts())) {
            doc->guessAvramiParams();
            updateAvramiParamsDraw(doc->getCCT(), doc->getAvramiParams());
            updateAvramiTable(doc->getAvramiParams());

            PanelSwitcher->setCurrentWidget(AvramiTab);
        }
    }
}

void Window::on_applyAvramiButton_clicked()
{
    if (avramiTable != nullptr)
        if (doc->setAvramiParams(avramiTable->getAvramiParams()))
            updateAvramiParamsDraw(doc->getCCT(), doc->getAvramiParams());
}

void Window::on_saveDocumentButton_clicked()
{
    QString path = QFileDialog::getSaveFileName();
    doc->writeToFile(path.toStdString());
}

void Window::on_openDocumentButton_clicked()
{
    QString path = QFileDialog::getOpenFileName();
    doc->readFromFile(path.toStdString());
    updateCCTDraw(doc->getCCT());
    updateCCTTable(doc->getCCT().experimentParams);

    PanelSwitcher->setCurrentWidget(CCTParamsTab);
}
